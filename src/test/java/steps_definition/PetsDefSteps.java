package steps_definition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.dependency.com.fasterxml.jackson.core.JsonProcessingException;
import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.ObjectMapper;
import logic.pets.*;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import templates.FieldValues;
import templates.MergeFrom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class PetsDefSteps {
    @Steps
    AddNewPetToStore newPet;
    @Steps
    AddNewPetResponse addNewPetResponse;
    @Steps
    FindPetClass findPet;
    @Steps
    UpdatePet updatePet;
    @Steps
    DeletePetClass deletePet;

    Object petId;

    String newPetRequestBoby;

    @Given("prepare new pet request body")
    public void prepare_new_pet_request_body(List<Map<String, String>> tradeDetails) throws IOException {
        newPetRequestBoby = MergeFrom.template("templates/new_pet/newPet.json")
                .withDefaultValuesFrom(FieldValues.in("templates/new_pet/standard-pet.properties"))
                .withFieldsFrom(tradeDetails.get(0));
    }

    @When("send pet creation request")
    public void send_pet_creation_request() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        petId = mapper.readValue(newPet.addNewPet(newPetRequestBoby), Map.class).get("id");
    }

    @When("Update pet field:")
    public void update_pet_field(List<Map<String, String>> fieldToUpdate) throws JsonProcessingException {
        updatePet.updatePetField(fieldToUpdate.get(0), petId);
    }

    @When("Remove pet from the store")
    public void remove_pet_from_the_store() {
        deletePet.deletePet(petId);
    }

    @When("^Find pet by status:$")
    public void find_pet_using_pet_status(List<Map<String, String>> petStatus) throws IOException {
        findPet.findPetByStatus(petStatus.get(0).get("status"));
    }


    @Then("pet was add to the store")
    public void pet_was_add_to_the_store(List<Map<String, String>> newPetDetails) {
        restAssuredThat(response ->
                response.statusCode(200));

        Map<String, String> expectedResponse = newPetDetails.get(0);
        Map<String, String> actualResponse = addNewPetResponse.returned();

        Assertions.assertThat(actualResponse).containsAllEntriesOf(expectedResponse);
    }

    @Then("Name must be new field:")
    public void name_must_be_new_field(List<Map<String, String>> expected) throws IOException {
        Object id = expected.get(0).get("id") == null ? petId : expected.get(0).get("id");
        findPet.findPetBuId(id);
        ObjectMapper mapper = new ObjectMapper();
        Assertions.assertThat(mapper.readValue(SerenityRest.lastResponse().getBody().asString(), Map.class))
                .containsAllEntriesOf(expected.get(0));
    }

    @Then("Could not get pet entity")
    public void could_not_get_pet_entity() {
        findPet.findPetBuId(petId);
        Assert.assertEquals(404, SerenityRest.lastResponse().getStatusCode());
    }

    @Then("^Get status code:$")
    public void get_status_code(List<Map<String, String>> expectedStatusCode) {
        Assert.assertEquals(Integer.parseInt(expectedStatusCode.get(0).get("statusCode")), SerenityRest.lastResponse().statusCode());
    }

    @Then("Find pet using pet ID")
    public void find_pet_by_id(List<Map<String, String>> expected) throws IOException {
        findPet.findPetBuId(petId);
        ObjectMapper mapper = new ObjectMapper();
        Map actualResponse = mapper.readValue(SerenityRest.lastResponse().getBody().asString(), Map.class);
        Assert.assertEquals(expected.get(0).get("name"), actualResponse.get("name"));
        Assert.assertEquals(petId, actualResponse.get("id"));
    }

    @Then("^All pet entry has searched status:$")
    public void check_searched_status(List<Map<String, String>> expectedStatus) throws IOException {
        List<String> resultList = new ArrayList<>();
        List<Map<String, ?>> resp = new ObjectMapper().readValue(
                SerenityRest.lastResponse().getBody().asString(),
                List.class
        );
        resp.forEach(map ->
                Assert.assertEquals(expectedStatus.get(0).get("status"), map.get("status")));

    }
}
