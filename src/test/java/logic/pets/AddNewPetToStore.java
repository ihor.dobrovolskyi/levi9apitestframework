package logic.pets;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class AddNewPetToStore {
    @Step("Add new pet to the store")
    public String addNewPet(String boby) {
        return SerenityRest.given()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .body(boby)
                .when()
                .post("https://petstore.swagger.io/v2/pet").andReturn().getBody().asString();
    }
}
