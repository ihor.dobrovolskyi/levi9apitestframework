package logic.pets;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class DeletePetClass {
    @Step("Delete pet from the store")
    public void deletePet(Object id) {
        SerenityRest.when().delete("https://petstore.swagger.io/v2/pet/" + id).then().statusCode(200);
    }
}
