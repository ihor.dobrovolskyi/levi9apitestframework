package logic.pets;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FindPetClass {
    @Step("Find pet pet by ID")
    public String findPetBuId(Object id) {
        return SerenityRest.given()
                .contentType(ContentType.JSON)
                .when()
                .get("https://petstore.swagger.io/v2/pet/" + id)
                .getBody().asString();
    }

    public List findPetByStatus(String petStatus) throws IOException {
        List<String> resultList = new ArrayList<>();
        List<Map<String, ?>> resp = new ObjectMapper().readValue(
                SerenityRest.given()
                        .queryParam("status", new String[]{petStatus})
                        .get("https://petstore.swagger.io/v2/pet/findByStatus").getBody().asString(),
                List.class
        );
        resp.forEach(map ->
                resultList.add((String) map.get("status")));
        return resultList;
    }
}
