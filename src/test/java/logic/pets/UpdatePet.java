package logic.pets;

import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.Map;
import java.util.Set;

public class UpdatePet {

    @Step("Update pet field")
    public void updatePetField(Map<String, ?> params, Object petId) {
        RequestSpecification req = null;
        Set<String> keys = params.keySet();
        for (int i = 0; i < keys.size(); i++) {
            req = SerenityRest.given()
                    .urlEncodingEnabled(true)
                    .formParam(keys.iterator().next(), params.get(keys.iterator().next()));
        }
        assert req != null;
        req.when()
                .post("https://petstore.swagger.io/v2/pet/" + petId)
                .then().statusCode(200);
    }
}