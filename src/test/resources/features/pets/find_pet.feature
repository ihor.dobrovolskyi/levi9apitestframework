Feature: Searching pets in the store

  Scenario: Find pet by ID
    Given prepare new pet request body
      | name          |
      | test_pet_name |
    When send pet creation request
    Then Find pet using pet ID
      | name          |
      | test_pet_name |

  Scenario: Find pet by status
    When Find pet by status:
      | status    |
      | available |
      | sold      |
      | pending   |
    Then All pet entry has searched status:
      | status    |
      | available |
      | sold      |
      | pending   |