Feature: Remove pet from the store

  Scenario: Remove pet from the store
    Given prepare new pet request body
      | name         |
      | testpet_name |
    When send pet creation request
    And Remove pet from the store
    Then Could not get pet entity