Feature: Add new pet

  Scenario: Add new pet to the store
    Given prepare new pet request body
      | name          |
      | testAvailable |
    When send pet creation request
    Then pet was add to the store
      | name          |
      | testAvailable |

  Scenario: Add new pet without name
    Given prepare new pet request body
      | name |
      | ""   |
    When send pet creation request
    Then Get status code:
      | statusCode |
      | 400        |