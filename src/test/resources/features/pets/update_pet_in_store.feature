Feature: Update pet

  Scenario: Update pet name
    Given prepare new pet request body
      | name         |
      | testpet_name |
    When send pet creation request
    When Update pet field:
      | name        |
      | updatedName |
    Then Name must be new field:
      | name        |
      | updatedName |

  Scenario: Update pet status
    Given prepare new pet request body
      | name         |
      | testpet_name |
    When send pet creation request
    When Update pet field:
      | status    |
      | available |
      | sold      |
      | pending   |
    Then Name must be new field:
      | name         | status    |
      | testpet_name | available |
      | testpet_name | sold      |
      | testpet_name | pending   |