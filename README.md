1. Clone repository `git clone https://gitlab.com/ihor.dobrovolskyi/levi9apitestframework.git`
2. Framework structure:
    1. Feature files has path: `/src/test/resources/features/`
    2. API colls has path: `src/test/java/logic`
    3. Steps definitions:  src/test/java/steps_definition
3. Run tests
    * `cd levi9apitestframework
    * ./gradlew clean test`
4. Tests executions results path:
    `build/serenity/reports`